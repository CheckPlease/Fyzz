import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {AngularFirestore} from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';

import { switchMap } from 'rxjs/operators';
import 'rxjs/add/operator/do';
import firebase from 'firebase/app';


@Injectable()
export class PaymentService {

    userId: string;
    stripeId: string;
    userInfo: any;
    membership: any;

    constructor(
      private db: AngularFireDatabase,
      private afAuth: AngularFireAuth,
      private afs: AngularFirestore){

        var user = firebase.auth().currentUser;

        console.log("userId = " + user.uid);
        this.afs.collection('User').doc(user.uid);
        this.userId = user.uid;

        if(user){
          var docRef = firebase.firestore().collection('User').doc(user.uid);
          docRef.get()
            .then(doc => {
              if (doc.exists) {
                console.log('Document data:', doc.data());
                this.userInfo = doc.data();
              } else {
                console.log('No such document!');
              }
            })
            .catch(function(error) {
              console.log('Error getting document:', error);
            });
        }
    }


    /*processPayment(token: any) {
      return this.afs.collection('User').doc(this.userId).collection('Token').doc('Token1').set({tokenId: token.id, customerId: this.userInfo.stripeId});
    }*/

    processPayment1290(token: any) {
      return this.afs.collection('User').doc(this.userId).collection('Token').doc('Token2').set({tokenId: token.id, customerId: this.userInfo.stripeId, aboType: '12.90'});
    }

    processPayment3490(token: any) {
      return this.afs.collection('User').doc(this.userId).collection('Token').doc('Token2').set({tokenId: token.id, customerId: this.userInfo.stripeId, aboType: '34.90'});
    }

    processPayment5990(token: any) {
      return this.afs.collection('User').doc(this.userId).collection('Token').doc('Token2').set({tokenId: token.id, customerId: this.userInfo.stripeId, aboType: '59.90'});
    }

    tokenUpdateCard(token: any) {
      return this.afs.collection('User').doc(this.userId).collection('Token').doc('TokenUpdate').set({tokenId: token.id, customerId: this.userInfo.stripeId});
    }
}
