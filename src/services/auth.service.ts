import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import AuthProvider = firebase.auth.AuthProvider;
import 'firebase/firestore'
import { createRendererType2 } from '@angular/core/src/view';
@Injectable()
export class AuthService {
	private user: firebase.User;

	constructor(public afAuth: AngularFireAuth) {
		afAuth.authState.subscribe(user => {
			this.user = user;
		});
	}


	signInWithEmail(credentials) {
		console.log('Sign in with email');
		return this.afAuth.auth.signInWithEmailAndPassword(credentials.email,
			 credentials.password);
	}


	signUp(credentials) {
	return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password)
	.then(newUserCredential => {
		console.log("newUserCreds = " + JSON.stringify(newUserCredential));
		firebase.firestore().collection("User").doc(newUserCredential.user['uid']).set({
			email:newUserCredential.user.email,
			postalCode:credentials.postalCode,
			birthdate:credentials.birthdate,
			city:credentials.city,
			firstname: credentials.firstname,
			lastname: credentials.lastname,
			sexe: credentials.sexe,
			counterDrink: 0,
			counterAdv: 0,
			scannerDrinkBool: true,
			scannerAdvBool: true,
			scannerAdvBoolSignup: true,
			fyzz_free: true,
			emailVerif: false
		});
	 })
	  .catch(error => {
		console.error("error is : " + error);
		throw new Error(error);
	  });;
	}
}
