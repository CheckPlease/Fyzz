import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, ToastController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { Subscription} from 'rxjs/Subscription';


@IonicPage()
@Component({
  selector: 'page-restaurant',
  templateUrl: 'restaurant.html',
})

export class RestaurantPage {
  userLang: string;

  networkConnection: boolean;
  networkConnected: Subscription;
  networkDisconnected: Subscription;

  restoAddress: string;
  restoCity: string;
  restoName: string;
  restoInfo: any;
  restoBeer: string;
  restoCoffee: string;
  restoMineral: string;
  restoCode: string;
  description: string;
  photoName1: string;
  photoName2: string;
  photoName3: string;
  photo1;
  photo2;
  photo3;
  restoId: string;

  restoInfoCollection: any;
  restoInfoObs: any;

  user: any;
  userRef: any;
  disableButton: boolean;
  scannerBool: boolean = false;
  scanText: string = '';

  public login: boolean = false;
  payment_status: string;

  options: BarcodeScannerOptions;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    private afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    private barcodeScanner: BarcodeScanner,
    public nativeStorage: NativeStorage,
    public network: Network) {

  }


  getLanguage() {
    this.nativeStorage.getItem('userSettings')
      .then(
        data => {
          this.userLang = data.language;
        },
        error => console.error(error)
      );
  }


  ionViewCanEnter(){
    this.getLanguage();

    this.restoInfo = this.navParams.data;
    this.restoInfoCollection = this.afs.collection<any>('Resto', ref =>
      ref.where('Name', '==', this.restoInfo.Name));
    this.restoInfoObs = this.restoInfoCollection.valueChanges();

    this.loadData();
    this.photoName1 = this.restoInfo.Name +'1';
    this.photoName2 = this.restoInfo.Name +'2';
    this.photoName3 = this.restoInfo.Name +'3';
    this.getPhotoURL();

    this.restoInfoCollection.ref.where('Name', '==', this.restoInfo.Name).get().then((querySnapshot) =>{
      querySnapshot.forEach((doc) => {
        this.restoBeer = doc.data().CodeB;
        this.restoCoffee = doc.data().CodeC;
        this.restoMineral = doc.data().CodeM;
        this.restoCode = doc.data().CodeVerif;
        this.restoId = doc.id;
      });
    });
  }


  // ionViewWillEnter(){
  //   this.networkMonitor();
  // }


  ionViewDidEnter(){
    this.networkMonitor();
  }


  ionViewWillLeave(){
    this.networkConnection = false;
    //console.log("The connection is " + this.networkConnection);
    this.networkConnected.unsubscribe();
    this.networkDisconnected.unsubscribe();
  }


  networkMonitor(){
    this.translate.get(['NETWORK.CONNECTION', 'NETWORK.DISCONNECT'])
    .subscribe(res => {
      this.networkConnected = this.network.onDisconnect().subscribe(() => {
        //console.log('network was disconnected :-(');
        this.networkConnection = false;
        this.toast.create({
          message: res["NETWORK.DISCONNECT"],
          duration: 5000
        }).present();
      });

      this.networkDisconnected = this.network.onConnect().subscribe(() => {
        setTimeout(() => {
          //console.log('we got a connection, woohoo!');
          this.networkConnection = true;
          this.toast.create({
            message: res["NETWORK.CONNECTION"] + this.network.type,
            duration: 5000
          }).present();
        }, 3000);
      });
    });
  }



  loadData(){
    this.afAuth.authState.subscribe(async userf => {
      this.user = userf;
      if(this.user)  {
        this.login = true;
        var paymentRef = await firebase.firestore().collection('User').doc(this.user.uid).collection('Stripe').doc('Abonnement');
        paymentRef.get().then(async doc => {
          if(doc.exists){
            this.payment_status = await doc.data().fyzz_standard;
            console.log("PAYMENT STATUS IS: " + this.payment_status);
            this.networkConnection = true;
            //console.log("The network in loadData is " + this.networkConnection);

            if(this.payment_status != 'active'){
              this.disableButton = false;
            }
          } else {
            console.log('No such document!');
          }
        });

        this.userRef = await firebase.firestore().collection('User').doc(this.user.uid);
        this.userRef.get().then(async doc => {
          this.scannerBool = await doc.data().scannerDrinkBool;
          if(this.scannerBool == true){
            this.disableButton = false;
            this.scanText = "scan_enable";
          }
          else if(this.scannerBool == false){
            this.disableButton = true;
            this.scanText = "scan_disable";
          }
        });
      } else {
        this.login = false;
      }
    });
  }


  openSignUp(){
    this.navCtrl.push('SignupPage');
  }


  openAboPage(){
      this.navCtrl.push('AbonnementPage');
  }


  openSettings(){
      this.navCtrl.push('SettingsPage');
  }


  openHome(){
      this.navCtrl.push('HomePage');
  }


  openContact(){
    this.navCtrl.push('ContactPage');
  }


  openScanner(){
    this.translate.get(['ABR.SCANQR','ABR.CANCEL','ABR.VERIFY','ABR.VERIFYTXTDRINK','ABR.VERIFYCODE','ABR.VERIFYENTERCODE','ABR.VERIFYCANCEL','ABR.VERIFYCANCELTXT','ABR.DRINKVALIDATED','ABR.DRINKVALIDATEDTXT','ABR.SCANCANCEL','ABR.SCANERROR','ABR.SCANERRORTXT','ABR.SCANACTIVATION','ABR.SCANACTIVATIONTXT','ABR.SCANSUBSCRIBE','ABR.SCANSUBSCRIBETXT', 'NETWORK.TURNONCONNECTION'])
    .subscribe(res => {
      this.options = {
        prompt: res["ABR.SCANQR"],
        resultDisplayDuration: 0,
        disableSuccessBeep: false
      }
      this.barcodeScanner.scan(this.options).then(barcodeData => {
        if(this.user){
          if(this.payment_status == 'active' && this.networkConnection == true){
            let qrCode = barcodeData.text.toString();
              if(this.restoBeer == qrCode || this.restoCoffee == qrCode || this.restoMineral == qrCode){
                let alert = this.alertCtrl.create({
                  title: res["ABR.VERIFY"],
                  enableBackdropDismiss: false,
                  subTitle: res["ABR.VERIFYTXTDRINK"],
                  inputs: [
                    {
                      name: "Code",
                      placeholder: res["ABR.VERIFYENTERCODE"],
                      type: 'password'
                    }
                  ],
                  buttons: [
                    {
                      text: res["ABR.CANCEL"],
                      role: 'cancel',
                      handler: data =>{
                        alert.dismiss('annule');
                        return false;
                      }
                    },
                    {
                      text: 'OK',
                      handler: data =>{
                        if(data.Code == this.restoCode){
                          alert.dismiss(true);
                          return false;
                        }else{
                          alert.dismiss(false);
                          return false;
                        }
                      }
                    }
                  ]
                });
                  alert.present();
                  alert.onDidDismiss((data)=>{
                    if(data==true && this.networkConnection == true){
                      let alert = this.alertCtrl.create({
                        title: res["ABR.DRINKVALIDATED"],
                        subTitle: res["ABR.DRINKVALIDATEDTXT"],
                        buttons: ['OK']
                      });
                      alert.present();
                      //Increment the user counter
                      // var user = firebase.auth().currentUser;
                      // var userRef =
                      //     firebase.firestore().collection('User').doc(user.uid);
                      var transaction = firebase.firestore()
                      .runTransaction(t => {
                        return t.get(this.userRef).then(doc => {
                          var newDrink = doc.data().counterDrink + 1;
                          t.update(this.userRef, { "counterDrink": newDrink, "scannerDrinkBool": false});
                          this.disableButton = true;
                          // this.scanText = "Tu As Déjà Bu!";
                          });
                        })
                      // increment the resto counter
                      if(this.restoBeer == qrCode){
                        var restoRef = firebase.firestore().collection("Resto").doc(this.restoId);
                        var transaction = firebase.firestore()
                        .runTransaction(t => {
                          return t.get(restoRef).then(doc => {
                            var newDrinkB = doc.data().Counter.Beer + 1;
                            t.update(restoRef, { "Counter.Beer": newDrinkB });
                          });
                        })
                      }
                      if(this.restoCoffee == qrCode){
                        var restoRef = firebase.firestore().collection("Resto").doc(this.restoId);
                        var transaction = firebase.firestore()
                        .runTransaction(t => {
                          return t.get(restoRef).then(doc => {
                            var newDrinkC = doc.data().Counter.Coffee + 1;
                            t.update(restoRef, { "Counter.Coffee": newDrinkC });
                          });
                        })
                      }
                      if(this.restoMineral == qrCode){
                        var restoRef = firebase.firestore().collection("Resto").doc(this.restoId);
                        var transaction = firebase.firestore()
                        .runTransaction(t => {
                          return t.get(restoRef).then(doc => {
                            var newDrinkM = doc.data().Counter.Mineral + 1;
                            t.update(restoRef, { "Counter.Mineral": newDrinkM });
                          });
                        })
                      }
                    } else {
                      if(data == false){
                        let alert = this.alertCtrl.create({
                          title: res["ABR.VERIFYCANCEL"],
                          subTitle: res["ABR.VERIFYCANCELTXT"],
                          buttons: ['OK']
                        });
                        alert.present();
                      } else if(this.networkConnection == false){
                        let alert = this.alertCtrl.create({
                          title: "ERROR 404",
                          subTitle: res["NETWORK.TURNONCONNECTION"],
                          buttons: ['OK']
                        });
                        alert.present();
                      } else {
                        if('cancel'){
                          let alert = this.alertCtrl.create({
                            title: res["ABR.VERIFYCANCEL"],
                            buttons: ['OK']
                          });
                          alert.present();
                        }
                      }
                    }
                  })
                } else {
                  if(barcodeData.cancelled == true){
                    let alert = this.alertCtrl.create({
                      title: res["ABR.SCANCANCEL"],
                      buttons: ['OK']
                    });
                    alert.present();
                }else{
                  let alert = this.alertCtrl.create({
                    title: res["ABR.SCANERROR"],
                    subTitle: res["ABR.SCANERRORTXT"],
                    buttons: ['OK']
                  });
                  alert.present();
                }
              }

            console.log('Barcode data', barcodeData);
          }
          else if (this.networkConnection == false){
            let alert = this.alertCtrl.create({
              title: "ERROR 404",
              subTitle: res["NETWORK.TURNONCONNECTION"],
              buttons: ['OK']
            });
            alert.present();
          }
          else{
            let alert = this.alertCtrl.create({
              title: res["ABR.SCANACTIVATION"],
              subTitle: res["ABR.SCANACTIVATIONTXT"],
              buttons: ['OK']
            });
            alert.present();
          }
        }
        else{
          let alert = this.alertCtrl.create({
            title: res["ABR.SCANSUBSCRIBE"],
            subTitle: res["ABR.SCANSUBSCRIBETXT"],
            buttons: ['OK']
          });
          alert.present();
        }

      }).catch(err => {
          console.log('Error', err);
      });
    });
  }


  getPhotoURL(){
    firebase.storage().ref().child('Resto/'+this.photoName1+'.jpg').getDownloadURL()
    .then((url) =>{
      this.photo1 = url;
    })
    firebase.storage().ref().child('Resto/'+this.photoName2+'.jpg').getDownloadURL()
    .then((url) =>{
      this.photo2 = url;
    })
    firebase.storage().ref().child('Resto/'+this.photoName3+'.jpg').getDownloadURL()
    .then((url) =>{
      this.photo3 = url;
    })
  }
}
