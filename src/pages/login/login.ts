import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';


@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  loginError: string;

  passwordShown: boolean = false;
  passwordType: string = 'password';

  constructor(
    private navCtrl: NavController,
    public translate: TranslateService,
    private auth: AuthService,
    fb: FormBuilder){
      this.loginForm = fb.group({
          email: ['', Validators.compose([Validators.required, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')])],
          password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
      });
  }


  public togglePassword(){
    if(this.passwordShown){
      this.passwordShown = false;
      this.passwordType = 'password';
    } else{
      this.passwordShown = true;
      this.passwordType = 'text';
    }
  }


  login() {
    let data = this.loginForm.value;

    if (!data.email) {
        return;
    }

    let credentials = {
        email: data.email,
        password: data.password
    };
    this.auth.signInWithEmail(credentials)
    .then(() => this.navCtrl.setRoot('MapPage'),
      error => {
        this.translate.get(['LOGIN.ERRORINVALIDEMAIL','LOGIN.ERRORUSERDISABLED','LOGIN.ERRORUSERNOTFOUND','LOGIN.ERRORWRONGPWD'])
        .subscribe(res => {
          if(error.code === 'auth/invalid-email' ) {
            this.loginError = res["LOGIN.ERRORINVALIDEMAIL"];
          }
          if(error.code === 'auth/user-disabled' ) {
            this.loginError = res["LOGIN.ERRORUSERDISABLED"];
          }
          if(error.code === 'auth/user-not-found' ) {
            this.loginError = res["LOGIN.ERRORUSERNOTFOUND"];
          }
          if(error.code === 'auth/wrong-password' ) {
            this.loginError = res["LOGIN.ERRORWRONGPWD"];
          }
        });
      }
    );
  }


  signup(){
    this.navCtrl.push('SignupPage');
  }


  openResetPassword(){
    this.navCtrl.push('ResetPasswordPage');
  }
}
