import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import firebase from 'firebase';


@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  // barcollection: AngularFirestoreCollection<any> = this.afs.collection('Bar');
  // barobs: any;
  // advcollection: AngularFirestoreCollection<any> = this.afs.collection('Advantage');
  // advobs: any;
  // restaurantcollection: AngularFirestoreCollection<any> =this.afs.collection('Resto');
  // restaurantobs: any;

  arrayQuery: Array<any> = [];
  array2: Array<any> = [];
  searchTerm: string = '';
  searching: boolean = false;

  constructor(
    private afs: AngularFirestore,
    public navCtrl: NavController,
    public navParams: NavParams) {
      this.arrayQuery = this.navParams.data;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }


  openSettings(){
    this.navCtrl.push('SettingsPage');
  }


  openPage(a, b){
    this.navCtrl.push(a, b);
  }


  filterItems(searchTerm){
    return this.arrayQuery.filter((item) => {
        return item.Name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }


  setFilteredItems() {
    this.array2 = this.filterItems(this.searchTerm);
    if(this.array2[0] != null){
      this.searching = true;
    }
    console.log(this.array2);
  }

}
